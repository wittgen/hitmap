#pragma once

#include <cstdint>
#include <array>
#include <bitset>
#include <memory>

template<bool USE_LUT=true>
class HitMapImpl {
public:
    HitMapImpl() = default;
    ~HitMapImpl() = default;
    static inline constexpr uint32_t LUT_LEN = 16;
    static inline constexpr uint32_t encode(uint32_t decoded, uint32_t &encoded) {
        if constexpr (USE_LUT) {
            uint64_t enc = lut_encode[decoded & 0xffff];
            encoded = enc & 0xffffffff;
            return enc >> 32;
        } else return _encode(decoded, encoded);
    }

    static inline constexpr uint32_t decode(uint32_t encoded, uint32_t &decoded) {
        if constexpr (USE_LUT) {
            auto _encoded = encoded & 0x3fffffff;
            auto [_decoded, _offset, _length] = lut1[_encoded >> (30 - LUT_LEN)];
            uint32_t hitmap = _decoded;
            decoded = hitmap;
            if (_offset == 0xff) return _length;
            _encoded = (_encoded >> _offset) & 0x3fff;
            auto [_hitmap, length2] = lut2[_encoded];
            decoded &= 0xff;
            decoded |= uint16_t(_hitmap) << 8;

            return _length + length2;
        } else return _decode(encoded, decoded);
    }

private:
    using LUT1_entry = std::tuple<uint16_t, uint8_t, uint8_t>;
    using LUT2_entry = std::tuple<uint8_t, uint8_t>;
    using LUT1_type = std::conditional_t<USE_LUT, std::array<LUT1_entry, 1 << LUT_LEN>, void *>;
    using LUT2_type = std::conditional_t<USE_LUT, std::array<LUT2_entry, 1 << (30 - LUT_LEN)>, void *>;
    using LUT_ENCODE_type = std::conditional_t<USE_LUT, std::array<uint64_t, (1 << 16)>, void *>;

    static inline constexpr auto _create_lut1() {
        LUT1_type lut;
        for (uint32_t i = 0; i < (1 << LUT_LEN); i++) {
            uint32_t decoded{};
            auto length = _decode(i << (30 - LUT_LEN), decoded);
            uint32_t offset = 0xff;
            if (length > 16) {
                uint32_t shift = decoded & 0xff;
                uint32_t encoded;
                length = _encode(shift, encoded);
                offset = 16 - length;
            }
            lut[i] = {decoded & 0xffff, offset & 0xff, length & 0xff};
        }
        return lut;
    };

    static inline LUT1_type lut1{_create_lut1()};

    static inline constexpr auto _create_lut2() {
        LUT2_type lut;
        for (uint32_t i = 0; i < 1 << (30 - LUT_LEN); i++) {
            uint32_t decoded{};
            auto length = _encode_bottom(i<<LUT_LEN, decoded);
            lut[i] = {decoded>>8, length};
        }
        return lut;
    }

    static inline LUT2_type lut2{_create_lut2()};

    static inline constexpr auto _create_lut_encode() {
        LUT_ENCODE_type lut;
        for (uint32_t i = 1; i < 1 << 16; i++) {
            uint32_t enc;
            uint64_t len = _encode(i, enc);
            lut[i] = enc | (len << 32);
        }
        return lut;
    }

    static inline LUT_ENCODE_type lut_encode{_create_lut_encode()};


    static inline constexpr uint32_t _encode(uint32_t decoded, uint32_t &encoded) {
        uint32_t Mbrr = ((decoded >> 14) & 0x1) << 1 | ((decoded >> 15) & 0x1);
        uint32_t Mbrl = ((decoded >> 12) & 0x1) << 1 | ((decoded >> 13) & 0x1);
        uint32_t Mblr = ((decoded >> 10) & 0x1) << 1 | ((decoded >> 11) & 0x1);
        uint32_t Mbll = ((decoded >> 8) & 0x1) << 1 | ((decoded >> 9) & 0x1);
        uint32_t Mtrr = ((decoded >> 6) & 0x1) << 1 | ((decoded >> 7) & 0x1);
        uint32_t Mtrl = ((decoded >> 4) & 0x1) << 1 | ((decoded >> 5) & 0x1);
        uint32_t Mtlr = ((decoded >> 2) & 0x1) << 1 | ((decoded >> 3) & 0x1);
        uint32_t Mtll = ((decoded >> 0) & 0x1) << 1 | ((decoded >> 1) & 0x1);

        uint32_t S1 = (bool(Mtll or Mtlr or Mtrl or Mtrr) << 1) | bool(Mbll or Mblr or Mbrl or Mbrr);
        uint32_t S2t = (bool(Mtll or Mtlr) << 1) | bool(Mtrl or Mtrr);
        uint32_t S2b = (bool(Mbll or Mblr) << 1) | bool(Mbrl or Mbrr);
        uint32_t S3tl = (bool(Mtll) << 1) | bool(Mtlr);
        uint32_t S3tr = (bool(Mtrl) << 1) | bool(Mtrr);
        uint32_t S3bl = (bool(Mbll) << 1) | bool(Mblr);
        uint32_t S3br = (bool(Mbrl) << 1) | bool(Mbrr);

        uint32_t pos = 0;
        encoded = 0;

        if (S1) writeTwo(S1, pos, encoded);
        if (S2t) writeTwo(S2t, pos, encoded);
        if (S3tl) writeTwo(S3tl, pos, encoded);
        if (S3tr) writeTwo(S3tr, pos, encoded);
        if (Mtll) writeTwo(Mtll, pos, encoded);
        if (Mtlr) writeTwo(Mtlr, pos, encoded);
        if (Mtrl) writeTwo(Mtrl, pos, encoded);
        if (Mtrr) writeTwo(Mtrr, pos, encoded);
        if (S2b) writeTwo(S2b, pos, encoded);
        if (S3bl) writeTwo(S3bl, pos, encoded);
        if (S3br) writeTwo(S3br, pos, encoded);
        if (Mbll) writeTwo(Mbll, pos, encoded);
        if (Mblr) writeTwo(Mblr, pos, encoded);
        if (Mbrl) writeTwo(Mbrl, pos, encoded);
        if (Mbrr) writeTwo(Mbrr, pos, encoded);

        return pos;
    }

    static constexpr inline uint32_t _decode(uint32_t encoded, uint32_t &decoded) {
        uint32_t pos{};
        uint32_t S1{};
        uint32_t S2t{};
        uint32_t S3tl{};
        uint32_t S3tr{};
        uint32_t Mtll{};
        uint32_t Mtlr{};
        uint32_t Mtrl{};
        uint32_t Mtrr{};
        uint32_t S2b{};
        uint32_t S3bl{};
        uint32_t S3br{};
        uint32_t Mbll{};
        uint32_t Mblr{};
        uint32_t Mbrl{};
        uint32_t Mbrr{};
        readTwo(encoded, pos, S1);
        switch (S1 & 3) {
            case 0b10:
                readTwo(encoded, pos, S2t);
                switch (S2t & 3) {
                    case 0b10:
                        readTwo(encoded, pos, S3tl);
                        if (S3tl == 0b10) {
                            readTwo(encoded, pos, Mtll);
                        } else if (S3tl == 0b01) {
                            readTwo(encoded, pos, Mtlr);
                        } else if (S3tl == 0b11) {
                            readTwo(encoded, pos, Mtll);
                            readTwo(encoded, pos, Mtlr);
                        }
                        break;
                    case 0b01:
                        readTwo(encoded, pos, S3tr);
                        if (S3tr == 0b10) {
                            readTwo(encoded, pos, Mtrl);
                        } else if (S3tr == 0b01) {
                            readTwo(encoded, pos, Mtrr);
                        } else if (S3tr == 0b11) {
                            readTwo(encoded, pos, Mtrl);
                            readTwo(encoded, pos, Mtrr);
                        }
                        break;
                    case 0b11:
                        readTwo(encoded, pos, S3tl);
                        readTwo(encoded, pos, S3tr);
                        if (S3tl == 0b10) {
                            readTwo(encoded, pos, Mtll);
                        } else if (S3tl == 0b01) {
                            readTwo(encoded, pos, Mtlr);
                        } else if (S3tl == 0b11) {
                            readTwo(encoded, pos, Mtll);
                            readTwo(encoded, pos, Mtlr);
                        }
                        if (S3tr == 0b10) {
                            readTwo(encoded, pos, Mtrl);
                        } else if (S3tr == 0b01) {
                            readTwo(encoded, pos, Mtrr);
                        } else if (S3tr == 0b11) {
                            readTwo(encoded, pos, Mtrl);
                            readTwo(encoded, pos, Mtrr);
                        }
                        break;
                }
                break;
            case 0b01:
                readTwo(encoded, pos, S2b);
                switch (S2b & 3) {
                    case 0b10:
                        readTwo(encoded, pos, S3bl);
                        if (S3bl == 0b10) {
                            readTwo(encoded, pos, Mbll);
                        } else if (S3bl == 0b01) {
                            readTwo(encoded, pos, Mblr);
                        } else if (S3bl == 0b11) {
                            readTwo(encoded, pos, Mbll);
                            readTwo(encoded, pos, Mblr);
                        }
                        break;
                    case 0b01:
                        readTwo(encoded, pos, S3br);
                        if (S3br == 0b10) {
                            readTwo(encoded, pos, Mbrl);
                        } else if (S3br == 0b01) {
                            readTwo(encoded, pos, Mbrr);
                        } else if (S3br == 0b11) {
                            readTwo(encoded, pos, Mbrl);
                            readTwo(encoded, pos, Mbrr);
                        }
                        break;
                    case 0b11:
                        readTwo(encoded, pos, S3bl);
                        readTwo(encoded, pos, S3br);
                        if (S3bl == 0b10) {
                            readTwo(encoded, pos, Mbll);
                        } else if (S3bl == 0b01) {
                            readTwo(encoded, pos, Mblr);
                        } else if (S3bl == 0b11) {
                            readTwo(encoded, pos, Mbll);
                            readTwo(encoded, pos, Mblr);
                        }
                        if (S3br == 0b10) {
                            readTwo(encoded, pos, Mbrl);
                        } else if (S3br == 0b01) {
                            readTwo(encoded, pos, Mbrr);
                        } else if (S3br == 0b11) {
                            readTwo(encoded, pos, Mbrl);
                            readTwo(encoded, pos, Mbrr);
                        }
                        break;
                }
                break;
            case 0b11:
                readTwo(encoded, pos, S2t);
                switch (S2t & 3) {
                    case 0b10:
                        readTwo(encoded, pos, S3tl);
                        if (S3tl == 0b10) {
                            readTwo(encoded, pos, Mtll);
                        } else if (S3tl == 0b01) {
                            readTwo(encoded, pos, Mtlr);
                        } else if (S3tl == 0b11) {
                            readTwo(encoded, pos, Mtll);
                            readTwo(encoded, pos, Mtlr);
                        }
                        break;
                    case 0b01:
                        readTwo(encoded, pos, S3tr);
                        if (S3tr == 0b10) {
                            readTwo(encoded, pos, Mtrl);
                        } else if (S3tr == 0b01) {
                            readTwo(encoded, pos, Mtrr);
                        } else if (S3tr == 0b11) {
                            readTwo(encoded, pos, Mtrl);
                            readTwo(encoded, pos, Mtrr);
                        }
                        break;
                    case 0b11:
                        readTwo(encoded, pos, S3tl);
                        readTwo(encoded, pos, S3tr);
                        if (S3tl == 0b10) {
                            readTwo(encoded, pos, Mtll);
                        } else if (S3tl == 0b01) {
                            readTwo(encoded, pos, Mtlr);
                        } else if (S3tl == 0b11) {
                            readTwo(encoded, pos, Mtll);
                            readTwo(encoded, pos, Mtlr);
                        }
                        if (S3tr == 0b10) {
                            readTwo(encoded, pos, Mtrl);
                        } else if (S3tr == 0b01) {
                            readTwo(encoded, pos, Mtrr);
                        } else if (S3tr == 0b11) {
                            readTwo(encoded, pos, Mtrl);
                            readTwo(encoded, pos, Mtrr);
                        }
                        break;
                }
                readTwo(encoded, pos, S2b);
                switch (S2b & 3) {
                    case 0b10:
                        readTwo(encoded, pos, S3bl);
                        if (S3bl == 0b10) {
                            readTwo(encoded, pos, Mbll);
                        } else if (S3bl == 0b01) {
                            readTwo(encoded, pos, Mblr);
                        } else if (S3bl == 0b11) {
                            readTwo(encoded, pos, Mbll);
                            readTwo(encoded, pos, Mblr);
                        }
                        break;
                    case 0b01:
                        readTwo(encoded, pos, S3br);
                        if (S3br == 0b10) {
                            readTwo(encoded, pos, Mbrl);
                        } else if (S3br == 0b01) {
                            readTwo(encoded, pos, Mbrr);
                        } else if (S3br == 0b11) {
                            readTwo(encoded, pos, Mbrl);
                            readTwo(encoded, pos, Mbrr);
                        }
                        break;
                    case 0b11:
                        readTwo(encoded, pos, S3bl);
                        readTwo(encoded, pos, S3br);
                        if (S3bl == 0b10) {
                            readTwo(encoded, pos, Mbll);
                        } else if (S3bl == 0b01) {
                            readTwo(encoded, pos, Mblr);
                        } else if (S3bl == 0b11) {
                            readTwo(encoded, pos, Mbll);
                            readTwo(encoded, pos, Mblr);
                        }
                        if (S3br == 0b10) {
                            readTwo(encoded, pos, Mbrl);
                        } else if (S3br == 0b01) {
                            readTwo(encoded, pos, Mbrr);
                        } else if (S3br == 0b11) {
                            readTwo(encoded, pos, Mbrl);
                            readTwo(encoded, pos, Mbrr);
                        }
                        break;
                }
                break;
        }

        //build the decoded
        decoded = 0;
        decoded |= ((Mtll >> 1) & 0x1) << 0;
        decoded |= ((Mtll >> 0) & 0x1) << 1;
        decoded |= ((Mtlr >> 1) & 0x1) << 2;
        decoded |= ((Mtlr >> 0) & 0x1) << 3;
        decoded |= ((Mtrl >> 1) & 0x1) << 4;
        decoded |= ((Mtrl >> 0) & 0x1) << 5;
        decoded |= ((Mtrr >> 1) & 0x1) << 6;
        decoded |= ((Mtrr >> 0) & 0x1) << 7;
        decoded |= ((Mbll >> 1) & 0x1) << 8;
        decoded |= ((Mbll >> 0) & 0x1) << 9;
        decoded |= ((Mblr >> 1) & 0x1) << 10;
        decoded |= ((Mblr >> 0) & 0x1) << 11;
        decoded |= ((Mbrl >> 1) & 0x1) << 12;
        decoded |= ((Mbrl >> 0) & 0x1) << 13;
        decoded |= ((Mbrr >> 1) & 0x1) << 14;
        decoded |= ((Mbrr >> 0) & 0x1) << 15;

        return pos;
    }


    static inline constexpr uint32_t _encode_bottom(uint32_t encoded, uint32_t &decoded) {
        uint32_t pos{};
        uint32_t S2b{};
        uint32_t S3bl{};
        uint32_t S3br{};
        uint32_t Mbll{};
        uint32_t Mblr{};
        uint32_t Mbrl{};
        uint32_t Mbrr{};
        readTwo(encoded, pos, S2b);
        switch (S2b & 3) {
            case 0b10:
                readTwo(encoded, pos, S3bl);
                if (S3bl == 0b10) {
                    readTwo(encoded, pos, Mbll);
                } else if (S3bl == 0b01) {
                    readTwo(encoded, pos, Mblr);
                } else if (S3bl == 0b11) {
                    readTwo(encoded, pos, Mbll);
                    readTwo(encoded, pos, Mblr);
                }
                break;
            case 0b01:
                readTwo(encoded, pos, S3br);
                if (S3br == 0b10) {
                    readTwo(encoded, pos, Mbrl);
                } else if (S3br == 0b01) {
                    readTwo(encoded, pos, Mbrr);
                } else if (S3br == 0b11) {
                    readTwo(encoded, pos, Mbrl);
                    readTwo(encoded, pos, Mbrr);
                }
                break;
            case 0b11:
                readTwo(encoded, pos, S3bl);
                readTwo(encoded, pos, S3br);
                if (S3bl == 0b10) {
                    readTwo(encoded, pos, Mbll);
                } else if (S3bl == 0b01) {
                    readTwo(encoded, pos, Mblr);
                } else if (S3bl == 0b11) {
                    readTwo(encoded, pos, Mbll);
                    readTwo(encoded, pos, Mblr);
                }
                if (S3br == 0b10) {
                    readTwo(encoded, pos, Mbrl);
                } else if (S3br == 0b01) {
                    readTwo(encoded, pos, Mbrr);
                } else if (S3br == 0b11) {
                    readTwo(encoded, pos, Mbrl);
                    readTwo(encoded, pos, Mbrr);
                }
                break;
        }
        decoded = 0;
        decoded |= ((Mbll >> 1) & 0x1) << 8;
        decoded |= ((Mbll >> 0) & 0x1) << 9;
        decoded |= ((Mblr >> 1) & 0x1) << 10;
        decoded |= ((Mblr >> 0) & 0x1) << 11;
        decoded |= ((Mbrl >> 1) & 0x1) << 12;
        decoded |= ((Mbrl >> 0) & 0x1) << 13;
        decoded |= ((Mbrr >> 1) & 0x1) << 14;
        decoded |= ((Mbrr >> 0) & 0x1) << 15;
        return pos;
    }

    static inline constexpr void readTwo(uint32_t src, uint32_t &pos, uint32_t &dst) {
        uint32_t val = (src >> (28 - pos)) & 0x3;
        if (val == 0b00 or val == 0b01) {
            dst = 0b01;
            pos++;
        } else {
            dst = val;
            pos += 2;
        }
    }

    static inline constexpr void writeTwo(uint32_t src, uint32_t &pos, uint32_t &dst) {
        if (src == 0b01) {
            dst |= (0b0) << (28 - pos);
            pos++;
        } else {
            dst |= (src & 0x3) << (28 - pos);
            pos += 2;
        }
    }
};

using HitMap = HitMapImpl<true>;
using HitMapNoLUT = HitMapImpl<false>;