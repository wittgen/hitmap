pub struct HitMap {
    // Static is not allowed for structs in rust, hence variables are moved to methods where required
}

impl HitMap {
    pub fn encode(decoded: u32, encoded: &mut u32) -> uint32_t {
        const LUT: bool = true;
        const LUT_LEN: u16 = 16;
        if LUT {
            let enc = Self::_create_lut_encode()[usize::from(decoded &0xffff)];
            *encoded = (enc & 0xffff_ffff) as u32;
            return (enc >> 32) as u32;
        } else {
            return Self::_encode(decoded, encoded)
        }
    }

    fn _create_lut() -> Vec<u32> {
        const LUT_LEN: u16 = 16;
        let mut lut = vec![0; 1 << LUT_LEN];
        for i in 0..(1 << LUT_LEN) {
            let mut decoded = 0;
            let length = Self::_decode(i << (30 - LUT_LEN), &mut decoded);
            let mut offset = 0xff;
            if length > 16 {
                let shift = decoded & 0xff;
                let mut encoded = 0;
                offset = Self::_encode(shift, &mut encoded);
            }
            lut[i] = (decoded & 0xffff) | (offset << 24) | (length << 16);
        } 
        lut
    }

    fn _create_lut_encode() -> Vec<u64> {
        let mut lut = vec![0; 1 << 16];
        for i in 1..(1 << 16) {
            let mut enc = 0;
            let len = _encode(i as u32, &mut enc);
            lut[i] = enc | (len << 32);
        }
        lut
    }

    fn _encode(decoded: u32, encoded: &mut u32) -> uint32_t {
        let lut1 = Self::create_lut1();
        let lut2 = Self::create_lut2();
        let lut_encode = Self::_create_lut_encode();
        let mut Mbrr = ((decoded >> 14) & 0x1) << 1 | ((decoded >> 15) & 0x1);
        let mut Mbrl = ((decoded >> 12) & 0x1) << 1 | ((decoded >> 13) & 0x1);
        let mut Mblr = ((decoded >> 10) & 0x1) << 1 | ((decoded >> 11) & 0x1);
        let mut Mbll = ((decoded >> 8) & 0x1) << 1 | ((decoded >> 9) & 0x1);
        let mut Mtrr = ((decoded >> 6) & 0x1) << 1 | ((decoded >> 7) & 0x1);
        let mut Mtrl = ((decoded >> 4) & 0x1) << 1 | ((decoded >> 5) & 0x1);
        let mut Mtlr = ((decoded >> 2) & 0x1) << 1 | ((decoded >> 3) & 0x1);
        let mut Mtll = ((decoded >> 0) & 0x1) << 1 | ((decoded >> 1) & 0x1);

        let mut S1 = ((Mtll | Mtlr | Mtrl | Mtrr) << 1) | (Mbll | Mblr | Mbrl | Mbrr);
        let mut S2t = ((Mtll | Mtlr) << 1) | (Mtrl | Mtrr);
        let mut S2b = ((Mbll | Mblr) << 1) | (Mbrl | Mbrr);
        let mut S3tl = ((Mtll) << 1) | (Mtlr);
        let mut S3tr = ((Mtrl) << 1) | (Mtrr);
        let mut S3bl = ((Mbll) << 1) | (Mblr);
        let mut S3br = ((Mbrl) << 1) | (Mbrr);

        let mut pos = 0;
        *encoded = 0;

        if S1 != 0 { Self::write_two(S1, &mut pos, encoded) };
        if S2t != 0 { Self::write_two(S2t, &mut pos, encoded) };
        if S3tl != 0 { Self::write_two(S3tl, &mut pos, encoded) };
        if S3tr != 0 { Self::write_two(S3tr, &mut pos, encoded) };
        if Mtll != 0 { Self::write_two(Mtll, &mut pos, encoded) };
        if Mtlr != 0 { Self::write_two(Mtlr, &mut pos, encoded) };
        if Mtrl != 0 { Self::write_two(Mtrl, &mut pos, encoded) };
        if Mtrr != 0 { Self::write_two(Mtrr, &mut pos, encoded) };
        if S2b != 0 { Self::write_two(S2b, &mut pos, encoded) };
        if S3bl != 0 { Self::write_two(S3bl, &mut pos, encoded) };
        if S3br != 0 { Self::write_two(S3br, &mut pos, encoded) };
        if Mbll != 0 { Self::write_two(Mbll, &mut pos, encoded) };
        if Mblr != 0 { Self::write_two(Mblr, &mut pos, encoded) };
        if Mbrl != 0 { Self::write_two(Mbrl, &mut pos, encoded) };
        if Mbrr != 0 { Self::write_two(Mbrr, &mut pos, encoded) };

        pos
    }

    fn write_two(src: u32, pos: &mut u32, dst: &mut u32) {
        if src == 0b01 {
            *dst |= (0b0) << (28 - pos);
            *pos += 1;
        } else {
            *dst |= (src & 0x3) << (28 - pos);
            *pos += 2;
        }
    }
}