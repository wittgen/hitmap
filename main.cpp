
#include <iostream>
#include "hitmap.hpp"

int main() {
    uint32_t len1, len2, decoded1, decoded2;

    for (unsigned i = 0;  i < (1 << 30); i++) {
        len1 = HitMap::decode(i, decoded1);
        len2 = HitMapNoLUT::decode(i, decoded2);
        if (len1 != len2 || decoded1 != decoded2) {
            std::cout <<std::hex << i << " " << len1 << " " << len2 << "\n";
            std::cout << decoded1 << " " << decoded2 << "\n";
        }
    }
    for (unsigned i = 1;  i < (1 << 16); i++){
        uint32_t encoded{}, decoded{};
        len1 = HitMap ::encode(i, encoded);
        len2 = HitMap ::decode(encoded, decoded);
        if(i != decoded ) {
            std::cout << std::hex << i  << " " << decoded << " " << encoded << "\n";
        }
    }

    return 0;

}
